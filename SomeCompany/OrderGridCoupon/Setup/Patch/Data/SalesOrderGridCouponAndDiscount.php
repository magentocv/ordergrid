<?php

namespace SomeCompany\OrderGridCoupon\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Data patch for updating sales_order_grid table coupon_code and discount_amount columns
 * class SalesOrderGridCouponAndDiscount
 */
class SalesOrderGridCouponAndDiscount implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $connection = $this->moduleDataSetup->getConnection();
        $grid = $this->moduleDataSetup->getTable('sales_order_grid');
        $table = $this->moduleDataSetup->getTable('sales_order');

        $connection->query(
            $connection->updateFromSelect(
                $connection->select()
                    ->join(
                        $table,
                        sprintf('%s.entity_id = %s.entity_id', $grid, $table),
                        ['coupon_code', 'discount_amount']
                    ),
                $grid
            )
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
